# CMake SDL

Run SDL example using CMake build system!

```sh
# Navigate to project directory
cd /path/to/project/root/

# Build with CMake, the build folder will be created automatically
cmake -S . -B build
```
